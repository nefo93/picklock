<?php

// Run this file to crack the passwords.
// Please use an autoloader.
use app\locks\Hardlock3;
use app\locks\Lock1;
use app\locks\Lock2;
use app\Picklock;


spl_autoload_register(function ($path) {
	$path = str_replace('\\', '/', $path);
	include ('../'.$path.'.php');
});

$pickLock = new Picklock();
$lock1 = new Lock1();
$lock2 = new Lock2();
$lock3 = new Hardlock3();

//
$pickLock->unlock($lock1);
$pickLock->unlock($lock2);
$pickLock->unlock($lock3);
$pickLock->unlockAllLocks();
$pickLock->varDumpLockResults();