<?php

namespace app\locks;

use app\lib\Lock;

/**
 * Class Hardlock3
 * @package app\locks
 */
class Hardlock3 extends Lock
{
    /** @var string $key */
    protected $key = 'db3d405b10675998c030223177d42e71b4e7a312';
}