<?php

namespace app\locks;

use app\lib\Lock;

/**
 * Class Lock1
 * @package app\locks
 */
class Lock1 extends Lock
{
    /** @var string $key */
    protected $key = '520104c03ea9af438104b10a25423696afaa845a';
}