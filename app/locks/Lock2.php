<?php

namespace app\locks;

use app\lib\Lock;

/**
 * Class Lock2
 * @package app\locks
 */
class Lock2 extends Lock
{
    /** @var string $key */
    protected $key = '7dcb618c6762cb68fa75f4e06465026d3952bcc2';
}