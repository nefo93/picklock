<?php

namespace app\lib;

/**
 * Interface PicklockInterface
 * @package app\lib
 */
interface PicklockInterface
{
    /**
     * Set passwords
     *
     * @param Lock $lock
     * @return mixed|void
     */
    public function unlock(Lock $lock);

    /**
     * Open all passwords
     */
    public function unlockAllLocks();

    /**
     * Use this method to var_dump variable $locks
     */
    public function varDumpLockResults();
}