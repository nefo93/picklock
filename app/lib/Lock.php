<?php

namespace app\lib;

/**
 * Class Lock
 * @package app\lib
 */
abstract class Lock
{
    /** @var string $key */
    protected $key;

    /** @var int $falseAttempts */
    public $falseAttempts = 0;  // Count of attempts

    /**
     * @param $password
     * @return bool
     */
    public function open($password)
    {
        if (sha1($password) == $this->key) {
            return true;
        } else {
            $this->increments();

            return false;
        }
    }

    protected function increments(): void
    {
        $this->falseAttempts++;
    }

    /**
     * @return int
     */
    public function getFalseAttempts(): int
    {
        return $this->falseAttempts;
    }
}