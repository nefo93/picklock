<?php

namespace app;

use app\CompilationService\Compilation;
use app\DTO\ResultDTO;
use app\lib\Lock;
use app\lib\PicklockInterface;

/**
 * This class has to call out the lock objects by name. i.e. 'lock1' should call
 * the class Lock1. This should not be hardcoded and must support adding of
 * new locks later on.
 * Using rainbow tables is not allowed.
 */
class Picklock implements PicklockInterface
{
    // Don't change static fields
    private static $locks = array(
        'lock1' => array(
            'password' => null,            // cracked password
            'millisecondsToUnlock' => 0,    // how many milliseconds did the cracking take
            'falseAttempt' => 0            // count of tries
        ),
        'lock2' => array(
            'password' => null,
            'millisecondsToUnlock' => 0,
            'falseAttempt' => 0
        ),
        'hardlock3' => array(
            'password' => null,
            'millisecondsToUnlock' => 0,
            'falseAttempt' => 0
        )
    );

    const minSymbols = 2;    // hint
    const maxSymbols = 4;    // hint

    /** @var array $symbols */
    private $symbols = array(
        'a',
        'b',
        'c',
        'd',
        'e',
        'f',
        'g',
        'h',
        'i',
        'j',
        'k',
        'l',
        'm',
        'n',
        'o',
        'p',
        'q',
        'r',
        's',
        't',
        'u',
        'v',
        'õ',
        'ä',
        'ö',
        'ü',
        'x',
        'y'
    ); // see on kolmas ja viimane vihje

    /** @var array $listLocks */
    private $listLocks;

    /** @var Compilation $compilation */
    private $compilation;

    /**
     * Picklock constructor.
     */
    public function __construct()
    {
        $this->compilation = new Compilation();
    }

    /**
     * Set passwords
     *
     * @param Lock $lock
     * @return mixed|void
     */
    public function unlock(Lock $lock)
    {
        $this->listLocks[] = $lock;
    }

    /**
     * Open all passwords
     */
    public function unlockAllLocks(): void
    {
        foreach ($this->listLocks as $lock) {
            $this->compilation->startData();
            $this->compilation->compilation($this->symbols, self::maxSymbols, $lock);
            $this->setResult();
        }
    }

    /**
     * Use this method to var_dump variable $locks
     */
    public function varDumpLockResults()
    {
        var_dump(self::$locks);
    }

    /**
     * Set the value for the current password
     */
    private function setResult(): void
    {
        /** @var ResultDTO $resultDTO */
        $resultDTO = $this->compilation->getResult();
        self::$locks[$resultDTO->getClassName()] = $resultDTO->getArrayResult();
    }

}