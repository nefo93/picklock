<?php

namespace app\CompilationService\Contracts;

use app\DTO\ResultDTO;
use app\lib\Lock;

/**
 * Class Compilation
 * @package app\CompilationService\Contracts
 */
interface Compilation
{
    /**
     * @param array $symbols
     * @param int $len
     * @param Lock $lock
     * @param string $str
     * @return bool
     */
    public function compilation(array $symbols, int $len, Lock $lock, string $str = ''): bool;

    /**
     * @return ResultDTO
     */
    public function getResult(): ResultDTO;

    /**
     * Start data
     */
    public function startData(): void;
}