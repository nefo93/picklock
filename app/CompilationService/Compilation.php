<?php

namespace app\CompilationService;

use app\CompilationService\Contracts\Compilation as CompilationContract;
use app\DTO\ResultDTO;
use app\lib\Lock;

/**
 * Class Compilation
 * @package app\CompilationService
 */
class Compilation implements CompilationContract
{
    /** @var float $data */
    private $data;

    /** @var ResultDTO $resultDTO */
    private $resultDTO;

    /**
     * Compilation constructor.
     */
    public function __construct()
    {
        $this->resultDTO = new ResultDTO();
    }

    /**
     * @param array $symbols
     * @param int $len
     * @param Lock $lock
     * @param string $str
     * @return bool
     */
    public function compilation(array $symbols, int $len, Lock $lock, string $str = ''): bool
    {
        if ($len == 0) {
            return false;
        }

        foreach ($symbols as $symbol) {
            $password = $str . $symbol;
            if (strlen($str) > 0 && $lock->open($password)) {
                $this->setResult($password, $lock);

                return true;
            }

            $this->compilation($symbols, $len - 1, $lock, $password);
        }

        return true;
    }

    /**
     * @return ResultDTO
     */
    public function getResult(): ResultDTO
    {
        return $this->resultDTO;
    }

    /**
     * Start data
     */
    public function startData(): void
    {
        $this->data = microtime(true);
    }

    /**
     * @return float
     */
    private function getData()
    {
        return round((microtime(true) - $this->data) * 1000);
    }

    /**
     * @param string $password
     * @param Lock $lock
     */
    private function setResult(string $password, Lock $lock): void
    {
        $this->resultDTO->setPassword($password);
        $this->resultDTO->setMilliseconds($this->getData());
        $this->resultDTO->setFalseAttempt($lock->getFalseAttempts());
        $this->resultDTO->setClassName(get_class($lock));
    }
}