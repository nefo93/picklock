<?php

namespace app\DTO;

/**
 * Class ResultDTO
 * @package app\DTO
 */
class ResultDTO
{
    public const PASSWORD = 'password';
    public const MILLISECONDS = 'millisecondsToUnlock';
    public const FALSE_ATTEMPT = 'falseAttempt';

    /** @var string $password */
    private $password = '';

    /** @var int $milliseconds */
    private $milliseconds = 0;

    /** @var int $falseAttempt */
    private $falseAttempt = 0;

    /** @var string $className */
    private $className = '';

    /**
     * @param int $falseAttempt
     * @return ResultDTO
     */
    public function setFalseAttempt(int $falseAttempt): self
    {
        $this->falseAttempt = $falseAttempt;

        return $this;
    }

    /**
     * @return int
     */
    public function getFalseAttempt(): int
    {
        return $this->falseAttempt;
    }

    /**
     * @param int $milliseconds
     * @return ResultDTO
     */
    public function setMilliseconds(int $milliseconds): self
    {
        $this->milliseconds = $milliseconds;

        return $this;
    }

    /**
     * @return int
     */
    public function getMilliseconds(): int
    {
        return $this->milliseconds;
    }

    /**
     * @param string $password
     * @return ResultDTO
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $className
     * @return ResultDTO
     */
    public function setClassName(string $className): self
    {
        $this->className = mb_strtolower(array_pop(explode('\\', $className)));

        return $this;
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @return array
     */
    public function getArrayResult(): array
    {
        return [
            self::PASSWORD => $this->getPassword(),
            self::MILLISECONDS => $this->getMilliseconds(),
            self::FALSE_ATTEMPT => $this->getFalseAttempt()
        ];
    }
}